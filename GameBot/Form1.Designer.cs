﻿namespace GameBot
{
  partial class Form1
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.nud_Fault = new System.Windows.Forms.NumericUpDown();
      this.btn_Company = new System.Windows.Forms.Button();
      this.btn_Dungeon = new System.Windows.Forms.Button();
      this.btn_stop = new System.Windows.Forms.Button();
      this.cb_changeHeroSet = new System.Windows.Forms.CheckBox();
      this.cb_ScreenLogging = new System.Windows.Forms.CheckBox();
      this.b_cryptRepeat = new System.Windows.Forms.Button();
      this.b_DoomTowerRepeat = new System.Windows.Forms.Button();
      this.b_DoomTowerNext = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize) (this.nud_Fault)).BeginInit();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(106, 357);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(180, 43);
      this.button1.TabIndex = 0;
      this.button1.Text = "Test";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.Button1Click);
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(439, 351);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(183, 49);
      this.button2.TabIndex = 1;
      this.button2.Text = "Different";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.DifferentButton_Click);
      // 
      // nud_Fault
      // 
      this.nud_Fault.Location = new System.Drawing.Point(637, 353);
      this.nud_Fault.Maximum = new decimal(new int[] {255, 0, 0, 0});
      this.nud_Fault.Name = "nud_Fault";
      this.nud_Fault.Size = new System.Drawing.Size(62, 22);
      this.nud_Fault.TabIndex = 2;
      // 
      // btn_Company
      // 
      this.btn_Company.Location = new System.Drawing.Point(42, 36);
      this.btn_Company.Name = "btn_Company";
      this.btn_Company.Size = new System.Drawing.Size(181, 41);
      this.btn_Company.TabIndex = 3;
      this.btn_Company.Text = "Company Repeat";
      this.btn_Company.UseVisualStyleBackColor = true;
      this.btn_Company.Click += new System.EventHandler(this.btn_Company_Click);
      // 
      // btn_Dungeon
      // 
      this.btn_Dungeon.Location = new System.Drawing.Point(42, 98);
      this.btn_Dungeon.Name = "btn_Dungeon";
      this.btn_Dungeon.Size = new System.Drawing.Size(181, 42);
      this.btn_Dungeon.TabIndex = 4;
      this.btn_Dungeon.Text = "Dungeon Repeat";
      this.btn_Dungeon.UseVisualStyleBackColor = true;
      this.btn_Dungeon.Click += new System.EventHandler(this.btn_Dungeon_Click);
      // 
      // btn_stop
      // 
      this.btn_stop.Location = new System.Drawing.Point(426, 59);
      this.btn_stop.Name = "btn_stop";
      this.btn_stop.Size = new System.Drawing.Size(144, 56);
      this.btn_stop.TabIndex = 5;
      this.btn_stop.Text = "Stop";
      this.btn_stop.UseVisualStyleBackColor = true;
      this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
      // 
      // cb_changeHeroSet
      // 
      this.cb_changeHeroSet.Location = new System.Drawing.Point(260, 36);
      this.cb_changeHeroSet.Name = "cb_changeHeroSet";
      this.cb_changeHeroSet.Size = new System.Drawing.Size(139, 40);
      this.cb_changeHeroSet.TabIndex = 6;
      this.cb_changeHeroSet.Text = "Change Hero Set";
      this.cb_changeHeroSet.UseVisualStyleBackColor = true;
      this.cb_changeHeroSet.CheckStateChanged += new System.EventHandler(this.cb_changeHeroSet_CheckStateChanged);
      // 
      // cb_ScreenLogging
      // 
      this.cb_ScreenLogging.Location = new System.Drawing.Point(259, 82);
      this.cb_ScreenLogging.Name = "cb_ScreenLogging";
      this.cb_ScreenLogging.Size = new System.Drawing.Size(140, 32);
      this.cb_ScreenLogging.TabIndex = 7;
      this.cb_ScreenLogging.Text = "ScreenLogging";
      this.cb_ScreenLogging.UseVisualStyleBackColor = true;
      this.cb_ScreenLogging.CheckedChanged += new System.EventHandler(this.cb_ScreenLogging_CheckedChanged);
      // 
      // b_cryptRepeat
      // 
      this.b_cryptRepeat.Location = new System.Drawing.Point(44, 160);
      this.b_cryptRepeat.Name = "b_cryptRepeat";
      this.b_cryptRepeat.Size = new System.Drawing.Size(178, 43);
      this.b_cryptRepeat.TabIndex = 8;
      this.b_cryptRepeat.Text = "Crypt repeat";
      this.b_cryptRepeat.UseVisualStyleBackColor = true;
      this.b_cryptRepeat.Click += new System.EventHandler(this.b_cryptRepeat_Click);
      // 
      // b_DoomTowerRepeat
      // 
      this.b_DoomTowerRepeat.Location = new System.Drawing.Point(42, 221);
      this.b_DoomTowerRepeat.Name = "b_DoomTowerRepeat";
      this.b_DoomTowerRepeat.Size = new System.Drawing.Size(180, 39);
      this.b_DoomTowerRepeat.TabIndex = 9;
      this.b_DoomTowerRepeat.Text = "Doom Tower Repeat";
      this.b_DoomTowerRepeat.UseVisualStyleBackColor = true;
      this.b_DoomTowerRepeat.Click += new System.EventHandler(this.b_DoomTowerRepeat_Click);
      // 
      // b_DoomTowerNext
      // 
      this.b_DoomTowerNext.Location = new System.Drawing.Point(48, 280);
      this.b_DoomTowerNext.Name = "b_DoomTowerNext";
      this.b_DoomTowerNext.Size = new System.Drawing.Size(173, 38);
      this.b_DoomTowerNext.TabIndex = 10;
      this.b_DoomTowerNext.Text = "Doom Tower Next";
      this.b_DoomTowerNext.UseVisualStyleBackColor = true;
      this.b_DoomTowerNext.Click += new System.EventHandler(this.b_DoomTowerNext_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.b_DoomTowerNext);
      this.Controls.Add(this.b_DoomTowerRepeat);
      this.Controls.Add(this.b_cryptRepeat);
      this.Controls.Add(this.cb_ScreenLogging);
      this.Controls.Add(this.cb_changeHeroSet);
      this.Controls.Add(this.btn_stop);
      this.Controls.Add(this.btn_Dungeon);
      this.Controls.Add(this.btn_Company);
      this.Controls.Add(this.nud_Fault);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.button1);
      this.Name = "Form1";
      this.Text = "Form1";
      ((System.ComponentModel.ISupportInitialize) (this.nud_Fault)).EndInit();
      this.ResumeLayout(false);
    }

    private System.Windows.Forms.Button b_DoomTowerNext;

    private System.Windows.Forms.Button b_DoomTowerRepeat;

    private System.Windows.Forms.Button b_cryptRepeat;

    private System.Windows.Forms.CheckBox cb_ScreenLogging;

    private System.Windows.Forms.CheckBox cb_changeHeroSet;

    private System.Windows.Forms.Button btn_stop;

    private System.Windows.Forms.Button btn_Company;
    private System.Windows.Forms.Button btn_Dungeon;

    private System.Windows.Forms.NumericUpDown nud_Fault;

    private System.Windows.Forms.Button button2;

    private System.Windows.Forms.Button button1;

    #endregion
  }
}
using System.Drawing;

namespace GameBot.Models.StaticData
{
  public static class SearchZoneCollection
  {
    //Company
    public static readonly Rectangle CompanyRepeatBtn = new Rectangle(1008, 957, 91, 25);
    public static readonly Rectangle CompanyChangeTeamBtn = new Rectangle(1297, 955, 80, 32);
    public static readonly Rectangle MaxLvlLabel = new Rectangle(744, 427, 640, 15); // x - 744, 1009, 1274
    public static readonly Rectangle EmptyHeroPosition1 = new Rectangle(379, 249, 75, 75);
    public static readonly Rectangle EmptyHeroPosition2 = new Rectangle(379, 453, 75, 75);
    public static readonly Rectangle EmptyHeroPosition3 = new Rectangle(216, 351, 75, 75);
    //ToDo: create separate zone for frames and heroes
    public static readonly Rectangle CompanyHeroPoolTop = new Rectangle(17, 695, 1356, 10); // 701 for 1 star mask, 703 - for 2 stars
    public static readonly Rectangle CompanyHeroPoolBottom = new Rectangle(17, 861, 1356, 15); //868 - 1*, 870 - 2*

    public static readonly Rectangle CompanyHeroLvlTop = new Rectangle(17, 813, 1356, 10);
    public static readonly Rectangle CompanyHeroLvlBottom = new Rectangle(17, 980, 1356, 10);

    public static readonly Rectangle HeroPoolRightEdge = new Rectangle(1400, 800, 60, 60);
    public static readonly Rectangle HeroPoolLeftEdge = new Rectangle(40, 800, 60, 60);

    public static readonly Rectangle OutOfEnergy = new Rectangle(837,723,246,51);
    public static readonly Rectangle TavernHeroPool = new Rectangle(30,180,380,870);

    //Dungeon
    public static readonly Rectangle DungeonRepeatBtn = new Rectangle(994, 955, 91, 25);
    public static readonly Rectangle CryptRepeatBtn = new Rectangle(1100, 955, 90, 27);
    
    //DoomTower
    public static readonly Rectangle BastionButton = new Rectangle(72, 917, 60, 70);
    
    //Arena
    public static readonly Rectangle ArenaStartBtn = new Rectangle(73, 913, 60, 70);
  }
}
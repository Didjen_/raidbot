namespace GameBot.Models.StaticData
{
  //ToDo: remove unused
  //ToDo: group by location
  public static class FilePathCollection
  {
    //Buttons
    public const string CompanyRepeatBtn = "./Sources/1080p/company_repeat_btn.bmp";
    public const string CompanyChangeTeamBtn = "./Sources/1080p/ChangeTeam.png";
    public const string DungeonRepeatBtn = "./Sources/1080p/dungeon_repeat.bmp";
    public const string CryptRepeatBtn = "./Sources/1080p/crypt_repeat.png";

    //Labels
    public const string RewardLbl = "./Sources/1080p/reward_lbl.bmp";
    public const string VictoryLbl = "./Sources/1080p/victory_lbl.bmp";
    public const string EmptyHeroSlot = "./Sources/1080p/EmptyHeroPosition.png";
    public const string OneLvl = "./Sources/1080p/One.png"; 
    public const string ZeroInLvl = "./Sources/1080p/Zero.png";
    public const string TavernLvl1 = "./Sources/1080p/TavernLvl1.png";
    public const string TavernLvl0 = "./Sources/1080p/TavernLvl0.png";
    public const string MaxLvlLabel = "./Sources/1080p/MaxLvlLabel.png";
    public const string HeroPoolEnd = "./Sources/1080p/heroPoolEnd.png";
    public const string SelectedHeroFrame = "./Sources/1080p/selectedFrame.png";
    public const string Bastion = "./Sources/1080p/Bastion.png";
    public const string OutOfEnergy = "./Sources/1080p/CompanyOutOfEnergy.png";
    public const string TavernRedIndicator = "./Sources/1080p/YavernRedIndicator.png";

    //Arena
    public const string ArenaStartBtn = "./Sources/1080p/arenaStartBtn.png";
    
    //temp
    public const string Screen = "./screen.png";
    public const string Second = "./second.png";
    public const string First = "./first.png";
    public const string Temp = "./Sources/MaxLvl_Mask.png";
  }
}
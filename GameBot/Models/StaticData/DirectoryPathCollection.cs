namespace GameBot.Models.StaticData
{
  public static class DirectoryPathCollection
  {
    public const string TwoStarsMaxLvl = "./Sources/Dif/2Stars_MaxLvl";
    public const string MaxLvl = "./Sources/Dif/MaxLvl";
    public const string ChangeHeroLvl1 = "./Sources/Dif/ChangeHero/1stars";
    public const string ChangeHeroLvl2 = "./Sources/Dif/ChangeHero/2stars";
    public const string One = "./Sources/Dif/One";
    public const string Zero = "./Sources/Dif/Zero";
    public const string TavernLvl1 = "./Sources/Dif/TavernLvl1";
    public const string TavernLvl0 = "./Sources/Dif/TavernLvl0";
    public const string MaxLvlLabel = "./Sources/Dif/MaxLvlLeble";
  }
}
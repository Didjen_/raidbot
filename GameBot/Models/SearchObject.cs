using System.Drawing;

namespace GameBot.Models
{
  public class SearchObject
  {
    public SearchObject(string path, Rectangle searchZone)
    {
      Path = path;
      SearchZone = searchZone;
    }

    public string Path { get; set; }
    public Rectangle SearchZone { get; set; }
  }
}
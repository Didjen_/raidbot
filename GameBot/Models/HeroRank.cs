namespace GameBot.Models
{
  public enum HeroRank
  {
    OneStar = 1,
    TwoStars = 2,
    ThreeStars = 3,
    FourStars = 4,
    FiveStars = 5,
    SixStars = 6,
  }
}
namespace GameBot.Models
{
  public class ArenaEnemyInfo
  {
    public string Name { get; private set; }
    public int Power { get; private set; }

    public ArenaEnemyInfo(string name, int power)
    {
      Name = name;
      Power = power;
    }
  }
}
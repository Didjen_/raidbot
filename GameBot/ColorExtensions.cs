using System;
using System.Drawing;

namespace GameBot
{
  public static class ColorExtensions
  {
    public static bool IsEqualsTo(this Color first, Color second, int fault = 1)
    {
      if (fault >= byte.MaxValue)
      {
        throw new ArgumentException(
          $"Fault param can't be more than {byte.MaxValue}. Result always true. Current value: {fault}");
      }

      return Math.Abs(first.A - second.A) <= fault &&
             Math.Abs(first.R - second.R) <= fault &&
             Math.Abs(first.G - second.G) <= fault &&
             Math.Abs(first.B - second.B) <= fault;
    }
  }
}
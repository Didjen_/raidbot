using System;
using System.IO;
using System.Windows.Forms;

namespace GameBot
{
  static class Program
  {
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      log4net.Config.XmlConfigurator.Configure(new FileInfo(@"log4net.config"));
      Application.SetHighDpiMode(HighDpiMode.SystemAware);
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new Form1());
    }
  }
}
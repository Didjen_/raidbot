using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace GameBot.Extensions
{
  public static class ListCollectionExtension
  {
    public static IEnumerable<Point> FilterByDistance(this IEnumerable<Point> points, int minDistance)
    {
      var result = new List<Point>();
      foreach (var point in points)
      {
        if (result.Any(p => p.GetDistance(point) < minDistance))
        {
          continue;
        }

        result.Add(point);
      }

      return result;
    }

    public static int GetDistance(this Point first, Point second)
    {
      return Math.Abs(first.X - second.X) + Math.Abs(first.Y - second.Y);
    }
  }
}
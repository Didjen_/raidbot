using System.Drawing;

namespace GameBot.Extensions
{
  public static class BitmapExtension
  {
    public static Bitmap ToBlackAndWhite(this Bitmap input)
    {
      // создаём Bitmap для черно-белого изображения
      var output = new Bitmap(input.Width, input.Height);
      // перебираем в циклах все пиксели исходного изображения
      for (var j = 0; j < input.Height; j++)
      for (var i = 0; i < input.Width; i++)
      {
        // получаем (i, j) пиксель
        var pixel = input.GetPixel(i, j);
        // делаем цвет черно-белым (оттенки серого) - находим среднее арифметическое
        var newPixelColor = (pixel.R + pixel.G + pixel.B) / 3 > 255 / 2 ? Color.Black : Color.White;
        // добавляем его в Bitmap нового изображения
        output.SetPixel(i, j, newPixelColor);
      }

      // выводим черно-белый Bitmap в pictureBox2
      return output;
    }

    public static Bitmap SubBitmap(this Bitmap bmp, Rectangle subZone)
    {
      return bmp.Clone(subZone, bmp.PixelFormat);
    }
  }
}
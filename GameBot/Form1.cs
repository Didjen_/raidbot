﻿using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using GameBot.Extensions;
using GameBot.GameController;
using GameBot.Models;
using GameBot.Models.StaticData;
using GameBot.Services;

namespace GameBot
{
  public partial class Form1 : Form
  {
    private static readonly log4net.ILog Logger =
      log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

    private readonly CompanyController _companyController = new CompanyController();
    private readonly DungeonController _dungeonController = new DungeonController();
    private readonly CryptController _cryptController = new CryptController();
    private readonly DoomTowerController _doomTowerController = new DoomTowerController();
    private CancellationTokenSource _token = new CancellationTokenSource();

    public Form1()
    {
      InitializeComponent();
    }

    //ToDo: Create search view formC
    //ToDo: try to change fault calculation: fault as sum of dif, not dif for each color
    void Button1Click(object sender, EventArgs e)
    {
      // //Thread.Sleep(2000);
      var fault = 30;
      var back = new Bitmap(FilePathCollection.First);
      var image = new Bitmap(FilePathCollection.ArenaStartBtn);
      //
      // var subBmp = back.SubBitmap(ImageTextZoneCollection.MaxLvlLabel);
      // subBmp.Save("Temp.png");
      // var text = ImageTextReader.ReadText(subBmp);
      
      var allScreen = new Rectangle(0, 0, 2000, 2000);
      var specialSearchZone = SearchZoneCollection.MaxLvlLabel;
      var temp = ImageAnalyzer.GetAllInCome(back, image, allScreen);
      var filtered = temp.FilterByDistance(15);
      //
      // var temp2 = ImageAnalyzer.GetAllInCome(back, image, SearchZoneCollection.CompanyHeroLvlTop, fault)
      //   .Union(ImageAnalyzer.GetAllInCome(back, image, SearchZoneCollection.CompanyHeroLvlBottom, fault))
      //   .ToList();
      // var filtered2 = temp2.FilterByDistance(5);

       var rank = HeroRank.OneStar;
      // var availableHeroes = _companyController.GetAvailableHeroes(back, rank, out rank);
      // MouseEmulator.PressWithMove(availableHeroes.Last());
    }

    private void PictureUpdate(Bitmap bmp)
    {
      for (var fj = 0; fj < bmp.Height; fj++)
      {
        for (var fi = 0; fi < bmp.Width; fi++)
        {
          var bmpPxl = bmp.GetPixel(fi, fj);
          if (!bmpPxl.IsEqualsTo(Color.White, (int) nud_Fault.Value))
          {
            bmp.SetPixel(fi, fj, Color.Black);
          }
        }
      }
      bmp.Save("Updated.png");
    }

    private void DifferentButton_Click(object sender, EventArgs e)
    {
      ImageService.CreateMaskOfDifferentPictures(DirectoryPathCollection.MaxLvlLabel, (int) nud_Fault.Value);
    }

    private void btn_Company_Click(object sender, EventArgs e)
    {
      var task = _companyController.RepeatAllTimes(_token);
    }

    private void btn_Dungeon_Click(object sender, EventArgs e)
    {
      _dungeonController.RepeatAllTimes(_token);
    }

    private void b_DoomTowerRepeat_Click(object sender, EventArgs e)
    {
      _doomTowerController.RepeatAllTimes(_token);
    }

    private void b_DoomTowerNext_Click(object sender, EventArgs e)
    {
      _doomTowerController.GoNextAllTimes(_token);
    }

    private void b_cryptRepeat_Click(object sender, EventArgs e)
    {
      _cryptController.RepeatAllTimes(_token);
    }

    private void btn_stop_Click(object sender, EventArgs e)
    {
      _token.Cancel();
      _token = new CancellationTokenSource();
    }

    private void cb_changeHeroSet_CheckStateChanged(object sender, EventArgs e)
    {
      GameSettings.Current.Company.ChangeHeroSet = cb_changeHeroSet.Checked;
    }

    private void cb_ScreenLogging_CheckedChanged(object sender, EventArgs e)
    {
      GameSettings.Current.Logging.LogScreens = cb_ScreenLogging.Checked;
    }
  }
}
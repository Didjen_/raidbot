using System;
using System.Threading;
using System.Threading.Tasks;

namespace GameBot.GameController
{
  public abstract class BaseController
  {
    public Task RepeatAllTimes(CancellationTokenSource token)
    {
      return Task.Factory.StartNew(() =>
      {
        while (!token.IsCancellationRequested)
        {
          Do(token);
        }
      });
    }

    public Task GoNextAllTimes(CancellationTokenSource token)
    {
      return Task.Factory.StartNew(() =>
      {
        while (!token.IsCancellationRequested)
        {
          GoNext(token);
        }
      });
    }

    public void DoOnce(CancellationTokenSource token)
    {
      Do(token);
    }

    protected abstract void Do(CancellationTokenSource token);

    protected virtual void GoNext(CancellationTokenSource token)
    {
      throw new NotImplementedException();
    }
  }
}
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using GameBot.Extensions;
using GameBot.Models;
using GameBot.Models.StaticData;
using GameBot.Services;

namespace GameBot.GameController
{
  public class CompanyController : BaseController
  {
    private static readonly log4net.ILog Logger =
      log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

    private readonly Bitmap _repeatBtn = new Bitmap(FilePathCollection.CompanyRepeatBtn);
    private readonly Bitmap _outOfEnergy = new Bitmap(FilePathCollection.OutOfEnergy);

    private readonly IDictionary<CompanyHeroPosition, Rectangle> _maxLvlPositionToHeroSlotZone =
      new Dictionary<CompanyHeroPosition, Rectangle>()
      {
        {CompanyHeroPosition.First, SearchZoneCollection.EmptyHeroPosition1},
        {CompanyHeroPosition.Second, SearchZoneCollection.EmptyHeroPosition2},
        {CompanyHeroPosition.Third, SearchZoneCollection.EmptyHeroPosition3},
      };

    protected override void Do(CancellationTokenSource token)
    {
      var screen = ImageService.GetScreenBmp();

      while (!ImageAnalyzer.IsFirstIncludeSecond(screen, _repeatBtn, out _, SearchZoneCollection.CompanyRepeatBtn))
      {
        //Thread.Sleep(1000);
        screen.Dispose(); //ToDo: try to refactor several Dispose call
        screen = ImageService.GetScreenBmp();
      }

      Thread.Sleep(1500);
      screen.Dispose();
      screen = ImageService.GetScreenBmp();
      var maxLvlHeroPositions = GetMaxLevelPositions();
      if (maxLvlHeroPositions.Any())
      {
        if (GameSettings.Current.Company.ChangeHeroSet)
        {
          var isChanged = ChangeHeroSet(maxLvlHeroPositions, token);
          if (!isChanged || token.IsCancellationRequested)
          {
            token.Cancel();
            return;
          }

          MouseEmulator.PressWithMove(ClickAddressCollection.StartBattleBtn);
        }
        else
        {
          token.Cancel();
        }

        return;
      }

      if (token.IsCancellationRequested)
      {
        return;
      }

      KeyboardEmulator.PressButton(Keys.R);

      screen.Dispose();
      screen = ImageService.GetScreenBmp();
      if (ImageAnalyzer.IsFirstIncludeSecond(screen, _outOfEnergy, out _, SearchZoneCollection.OutOfEnergy))
      {
        KeyboardEmulator.PressButton(Keys.Escape);
        token.Cancel();
        return;
      }

      screen.Dispose();
      Thread.Sleep(10000);
    }

    private List<CompanyHeroPosition> GetMaxLevelPositions()
    {
      string[] splitParts;
      do
      {
        var screen = ImageService.GetScreenBmp();
        var subBmp = screen.SubBitmap(ImageTextZoneCollection.MaxLvlLabel);
        screen.Dispose();
        var text = ImageTextReader.ReadText(subBmp);
        splitParts = text.Split(" ", StringSplitOptions.RemoveEmptyEntries);
      } while (splitParts.Length != 6);

      var tuples = new List<(string, CompanyHeroPosition)>
      {
        ($"{splitParts[0]} {splitParts[1]}", CompanyHeroPosition.First),
        ($"{splitParts[2]} {splitParts[3]}", CompanyHeroPosition.Second),
        ($"{splitParts[4]} {splitParts[5]}", CompanyHeroPosition.Third)
      };

      return tuples
        .Where(t => t.Item1.Contains("Макс. уровень", StringComparison.OrdinalIgnoreCase))
        .Select(t => t.Item2)
        .ToList();
    }

    private bool ChangeHeroSet(IEnumerable<CompanyHeroPosition> maxLvlHeroPositions, CancellationTokenSource token)
    {
      Logger.Info($"Start ChangeHeroSet process");

      MouseEmulator.PressWithMove(ClickAddressCollection.ChangeTeamBtn);

      var screen = ImageService.GetScreenBmp();
      if (ImageAnalyzer.IsFirstIncludeSecond(screen, _outOfEnergy, out _, SearchZoneCollection.OutOfEnergy))
      {
        KeyboardEmulator.PressButton(Keys.Escape);
        screen.Dispose();
        return false;
      }

      ScrollHeroPoolToEnd();
      foreach (var position in maxLvlHeroPositions)
      {
        var isChanged = ChangeHero(_maxLvlPositionToHeroSlotZone[position]);
        if (!isChanged || token.IsCancellationRequested)
        {
          return false;
        }
      }

      return true;
    }

    private bool ChangeHero(Rectangle searchZone)
    {
      using var emptyHeroSlot = new Bitmap(FilePathCollection.EmptyHeroSlot);
      var clickCoordinate = new Point(searchZone.Left, searchZone.Top);
      Bitmap screen = null;
      MouseEmulator.PressWithMove(clickCoordinate);
      do
      {
        MouseEmulator.PressWithMove(clickCoordinate);
        screen?.Dispose();
        screen = ImageService.GetScreenBmp();
      } while (ImageAnalyzer.IsFirstIncludeSecond(screen, emptyHeroSlot, out _, searchZone));
      //ToDo: wrap ImageAnalyzer call with four params in separate Service. CompanyImageAnalyzer for example

      var nextHeroCoordinate = FindNextHero();
      Logger.Info($"Next hero coordinate: {nextHeroCoordinate}");
      if (!nextHeroCoordinate.HasValue)
      {
        return false;
      }

      do
      {
        MouseEmulator.PressWithMove(nextHeroCoordinate.Value);
        Thread.Sleep(200);
        screen?.Dispose();
        screen = ImageService.GetScreenBmp();
      } while (ImageAnalyzer.IsFirstIncludeSecond(screen, emptyHeroSlot, out _, searchZone));

      return true;
    }

    private Point? FindNextHero()
    {
      //ToDo: add cache to check is all one/two/three stars hero were scrolled
      using var selectedHeroFrame = new Bitmap(FilePathCollection.SelectedHeroFrame);
      using var oneLvl = new Bitmap(FilePathCollection.OneLvl);
      using var zeroInLvl = new Bitmap(FilePathCollection.ZeroInLvl);

      do
      {
        using var screen = ImageService.GetScreenBmp();
        var selectedHeroFrames = ImageAnalyzer
          .GetAllInCome(screen, selectedHeroFrame, SearchZoneCollection.CompanyHeroPoolTop)
          .Union(ImageAnalyzer.GetAllInCome(screen, selectedHeroFrame, SearchZoneCollection.CompanyHeroPoolBottom))
          .Select(p => new Rectangle(p, selectedHeroFrame.Size))
          .ToList();

        var zeroLvls = ImageAnalyzer
          .GetAllInCome(screen, selectedHeroFrame, SearchZoneCollection.CompanyHeroPoolTop)
          .Union(ImageAnalyzer.GetAllInCome(screen, selectedHeroFrame, SearchZoneCollection.CompanyHeroPoolBottom))
          .FilterByDistance(5)
          .ToList();

        var availableHeroes = ImageAnalyzer
          .GetAllInCome(screen, oneLvl, SearchZoneCollection.CompanyHeroLvlTop)
          .Union(ImageAnalyzer.GetAllInCome(screen, oneLvl, SearchZoneCollection.CompanyHeroLvlBottom))
          .FilterByDistance(5)
          .Where(p => selectedHeroFrames.All(r => !r.Contains(p))) // Not selected
          .Where(p => zeroLvls.All(zp => zp.GetDistance(p) > 15))
          .ToList();
        Logger.Info($"All available heroes positions: {string.Join(", ", availableHeroes)}");

        if (availableHeroes.Any())
        {
          Logger.Info($"Selected hero position: {availableHeroes.Last()}");
          return availableHeroes.Last();
        }
      } while (ScrollHeroPoolToLeftIsAvailable());

      return null;
    }

    private static void ScrollHeroPoolToEnd()
    {
      using var heroPoolEnd = new Bitmap(FilePathCollection.HeroPoolEnd);
      MouseEmulator.ScrollRight(300, ClickAddressCollection.CompanyHeroPool);
      while (true)
      {
        using var screen = ImageService.GetScreenBmp();
        if (ImageAnalyzer.IsFirstIncludeSecond(screen, heroPoolEnd, out _, SearchZoneCollection.HeroPoolRightEdge))
        {
          break;
        }

        MouseEmulator.ScrollRight(20, ClickAddressCollection.CompanyHeroPool);
      }
    }

    private static bool ScrollHeroPoolToLeftIsAvailable()
    {
      using var heroPoolEnd = new Bitmap(FilePathCollection.HeroPoolEnd);
      MouseEmulator.ScrollLeft(20, ClickAddressCollection.CompanyHeroPool);
      using var screen = ImageService.GetScreenBmp();
      if (ImageAnalyzer.IsFirstIncludeSecond(screen, heroPoolEnd, out _, SearchZoneCollection.HeroPoolLeftEdge))
      {
        return false;
      }

      return true;
    }
  }
}
using System;
using System.IO;
using Newtonsoft.Json;

namespace GameBot.GameController
{
  public static class GameSettings
  {
    public static string _configFilePath = "GameSetting.config";
    static GameSettings()
    {
      if (File.Exists(_configFilePath))
      {
        var json = File.ReadAllText(_configFilePath);
        Current = JsonConvert.DeserializeObject<GameSettingModel>(_configFilePath);
      }
      else
      {
        Current = new GameSettingModel();
      }
    }

    public static GameSettingModel Current { get; set; }
  }

  public class GameSettingModel: IDisposable
  {
    public CompanySettings Company { get; set; } = new CompanySettings();
    public LogSettings Logging { get; set; } = new LogSettings();

    public void Dispose()
    {
      var model = JsonConvert.SerializeObject(this);
      File.WriteAllText(GameSettings._configFilePath, model);
    }

    ~GameSettingModel()
    {
      Dispose();
    }
  }

  public class CompanySettings
  {
    public bool ChangeHeroSet { get; set; } = false;
  }

  public class LogSettings
  {
    public bool LogScreens { get; set; } = false;
  }
}
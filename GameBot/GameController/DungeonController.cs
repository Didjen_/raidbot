using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using GameBot.Models;
using GameBot.Models.StaticData;
using GameBot.Services;

namespace GameBot.GameController
{
  public class DungeonController : BaseController
  {
    private readonly Bitmap _repeatBtn = new Bitmap(FilePathCollection.DungeonRepeatBtn);

    protected override void Do(CancellationTokenSource token)
    {
      var screen = ImageService.GetScreenBmp();

      while (!ImageAnalyzer.IsFirstIncludeSecond(screen, _repeatBtn, out _, SearchZoneCollection.DungeonRepeatBtn) && !token.IsCancellationRequested)
      {
        Thread.Sleep(1000);
        screen.Dispose();
        screen = ImageService.GetScreenBmp();
      }

      if (token.IsCancellationRequested)
      {
        return;
      }

      screen.Dispose();
      KeyboardEmulator.PressButton(Keys.R);
    }
  }
}
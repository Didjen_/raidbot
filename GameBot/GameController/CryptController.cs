using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using GameBot.Models.StaticData;
using GameBot.Services;

namespace GameBot.GameController
{
  public class CryptController : BaseController
  {
    private readonly Bitmap _repeatBtn = new Bitmap(FilePathCollection.CryptRepeatBtn);

    protected override void Do(CancellationTokenSource token)
    {
      var screen = ImageService.GetScreenBmp();

      while (!ImageAnalyzer.IsFirstIncludeSecond(screen, _repeatBtn, out _, SearchZoneCollection.CryptRepeatBtn))
      {
        Thread.Sleep(1000);
        screen.Dispose();
        screen = ImageService.GetScreenBmp();
      }

      screen.Dispose();
      KeyboardEmulator.PressButton(Keys.R);
    }
  }
}
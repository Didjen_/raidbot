using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using GameBot.Models.StaticData;
using GameBot.Services;

namespace GameBot.GameController
{
  public class DoomTowerController : BaseController
  {
    private readonly Bitmap _bastionBtn = new Bitmap(FilePathCollection.Bastion);

    protected override void Do(CancellationTokenSource token)
    {
      var screen = ImageService.GetScreenBmp();

      while (!ImageAnalyzer.IsFirstIncludeSecond(screen, _bastionBtn, out _, SearchZoneCollection.BastionButton))
      {
        Thread.Sleep(1000);
        screen.Dispose();
        screen = ImageService.GetScreenBmp();
      }

      screen.Dispose();
      KeyboardEmulator.PressButton(Keys.R);
    }

    protected override void GoNext(CancellationTokenSource token)
    {
      var screen = ImageService.GetScreenBmp();

      while (!ImageAnalyzer.IsFirstIncludeSecond(screen, _bastionBtn, out _, SearchZoneCollection.BastionButton))
      {
        Thread.Sleep(1000);
        screen.Dispose();
        screen = ImageService.GetScreenBmp();
      }

      screen.Dispose();
      KeyboardEmulator.PressButton(Keys.Space);
      Thread.Sleep(500);
      MouseEmulator.PressWithMove(ClickAddressCollection.StartBattleBtn);
    }
  }
}
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace GameBot.Services
{
  public static class MouseEmulator
  {
    [DllImport("User32.dll")]
    private static extern void mouse_event(MouseFlags dwFlags, int dx, int dy, int dwData, UIntPtr dwExtraInfo);

    [Flags]
    enum MouseFlags
    {
      Move = 0x0001,
      LeftDown = 0x0002,
      LeftUp = 0x0004,
      RightDown = 0x0008,
      RightUp = 0x0010,
      Absolute = 0x8000,
      MiddleDown = 0x0020,
      MiddleUp = 0x0040,
      XDown = 0x0080,
      XUp = 0x0100,
      Wheel = 0x0800,
      HWheel = 0x01000, // Horizontal wheel
    }

    public static void PressWithMove(Point position)
    {
      // поставить курсор мыши в координаты x и y
      Cursor.Position = position;
      mouse_event(MouseFlags.LeftDown | MouseFlags.LeftUp, position.X, position.Y, 0, UIntPtr.Zero);
    }

    public static void ScrollUp(int steps, Point position)
    {
      Cursor.Position = position;
      //DwData - how many px to scroll. But Raid always see it as one scrollEvent (120px)
      for (int i = 0; i < steps; i++)
      {
        mouse_event(MouseFlags.Wheel, 0, 0, 120, UIntPtr.Zero);
        Thread.Sleep(20);
      }
    }

    public static void ScrollDown(int steps, Point position)
    {
      Cursor.Position = position;
      //DwData - how many px to scroll. But Raid always see it as one scrollEvent (120px)
      for (int i = 0; i < steps; i++)
      {
        mouse_event(MouseFlags.Wheel, 0, 0, -120, UIntPtr.Zero);
        Thread.Sleep(20);
      }
    }

    public static void ScrollRight(int steps, Point position)
    {
      Cursor.Position = position;
      //DwData - how many px to scroll. But Raid always see it as one scrollEvent (120px)
      for (int i = 0; i < steps; i++)
      {
        mouse_event(MouseFlags.HWheel, 0, 0, -120, UIntPtr.Zero);
        Thread.Sleep(20);
      }
    }

    public static void ScrollLeft(int steps, Point position)
    {
      Cursor.Position = position;
      //DwData - how many px to scroll. But Raid always see it as one scrollEvent (120px)
      for (int i = 0; i < steps; i++)
      {
        mouse_event(MouseFlags.HWheel, 0, 0, 120, UIntPtr.Zero);
        Thread.Sleep(20);
      }
    }

    // public static void DragAndDrop(DragAndDrop dragAndDrop)
    // {
    //   DragAndDropWithoutButtonUp(dragAndDrop);
    //   mouse_event(MouseFlags.LeftUp, dragAndDrop.To.X, dragAndDrop.To.Y, 0, UIntPtr.Zero);
    // }
    //
    // public static void DragAndDropWithoutButtonUp(DragAndDrop dragAndDrop)
    // {
    //   Cursor.Position = dragAndDrop.From;
    //   mouse_event(MouseFlags.LeftDown, dragAndDrop.From.X, dragAndDrop.From.Y, 0, UIntPtr.Zero);
    //   Thread.Sleep(300);
    //   mouse_event(MouseFlags.Move, dragAndDrop.To.X, dragAndDrop.To.Y, 0, UIntPtr.Zero);
    //   //Cursor.Position = dragAndDrop.To;
    // }
    //
    // public static void LeftUp()
    // {
    //   mouse_event(MouseFlags.LeftUp, Cursor.Position.X, Cursor.Position.Y, 0, UIntPtr.Zero);
    // }
  }
}
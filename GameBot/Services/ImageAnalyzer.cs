using System.Collections.Generic;
using System.Drawing;

namespace GameBot.Services
{
  //ToDo: try to move to separate project
  public static class ImageAnalyzer
  {
    //ToDo: Convert to extension to make it more readable
    public static bool IsFirstIncludeSecond(Bitmap firstBmp, Bitmap secondBmp, out Point coordinate, Rectangle searchZone)
    {
      return Compare(firstBmp, secondBmp, out coordinate, searchZone);
    }


    public static List<Point> GetAllInCome(Bitmap screen, Bitmap fragment, Rectangle searchZone, int fault = 20)
    {
      var maxI = screen.Width - fragment.Width > searchZone.Right ? searchZone.Right : screen.Width - fragment.Width;
      var maxJ = screen.Height - fragment.Height > searchZone.Bottom ? searchZone.Bottom : screen.Height - fragment.Height;
      var allInComeCoordinates = new List<Point>();
      for (var si = searchZone.Left; si <= maxI; si++)
      {
        for (var sj = searchZone.Top; sj <= maxJ; sj++)
        {
          if (CompareFragmentByCoordinate(screen, fragment, si, sj, fault))
          {
            allInComeCoordinates.Add(new Point(si, sj));
          }
        }
      }

      return allInComeCoordinates;
    }

    private static bool Compare(Bitmap screen, Bitmap fragment, out Point coordinate, Rectangle searchZone, int fault = 20)
    {
      var maxI = screen.Width - fragment.Width > searchZone.Right ? searchZone.Right : screen.Width - fragment.Width;
      var maxJ = screen.Height - fragment.Height > searchZone.Bottom ? searchZone.Bottom : screen.Height - fragment.Height;
      for (var si = searchZone.Left; si <= maxI; si++)
      {
        for (var sj = searchZone.Top; sj <= maxJ; sj++)
        {
          if (CompareFragmentByCoordinate(screen, fragment, si, sj, fault))
          {
            coordinate = new Point(si, sj);
            return true;
          }
        }
      }

      coordinate = Point.Empty;
      return false;
    }

    private static bool CompareFragmentByCoordinate(Bitmap screen, Bitmap fragment, int si, int sj, int fault)
    {
      for (var fj = 0; fj < fragment.Height; fj++, sj++)
      {
        for (var fi = 0; fi < fragment.Width; fi++, si++)
        {
          var fragmentPxl = fragment.GetPixel(fi, fj);
          var screenPxl = screen.GetPixel(si, sj);
          // Alpha = 0 for transparent pixel in mask
          if (fragmentPxl.A != 0 && screenPxl.A != 0 && !screenPxl.IsEqualsTo(fragmentPxl, fault)
          ) //ToDo: bring fault as an argument
          {
            return false;
          }
        }

        si -= fragment.Width; //Return carriage to start of the row
      }

      return true;
    }
  }
}
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using GameBot.GameController;

namespace GameBot.Services
{
  public static class ImageService // ToDo: create visual tool to get common mask png
  {
    public static void CreateMaskOfDifferentPictures(string directory, int fault = 10)
    {
      var d = new DirectoryInfo(directory);
      var files = d.GetFiles("*.png", SearchOption.TopDirectoryOnly);
      if (files.Length < 2)
      {
        throw new ArgumentException(
          $"There are too less files to make a mask. Directory: {directory}, Files: {string.Join(", ", files.Select(f => f.Name))}");
      }

      var result = new Bitmap(files[0].FullName);
      for (var i = 1; i < files.Length; i++)
      {
        var image = new Bitmap(files[i].FullName);
        result = CreateCommonPixelMask(result, image, fault);
      }

      var maskPath = directory.TrimEnd('\\') + "\\" + "mask.png";
      if (File.Exists(maskPath))
        File.Delete(maskPath);
      result.Save(maskPath);

      result.Dispose();
    }

    //ToDo: check better solution https://stackoverflow.com/questions/5049122/capture-the-screen-shot-using-net
    public static Bitmap GetScreenBmp()
    {
      var bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
      using (var g = Graphics.FromImage(bmpScreenCapture))
      {
        g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
          Screen.PrimaryScreen.Bounds.Y,
          0, 0,
          bmpScreenCapture.Size,
          CopyPixelOperation.SourceCopy);
      }

      if (GameSettings.Current.Logging.LogScreens)
      {
        var curDate = DateTime.Now.ToString("yyyy-MM-dd");
        var logScreenPath = $"log/{curDate}/Screens/{DateTime.Now.TimeOfDay.ToString().Replace(':','_')}.png";
        // var bmpScreenCaptureCopy = (Bitmap)bmpScreenCapture.Clone();
        // bmpScreenCaptureCopy.Save(logScreenPath);
      }
      bmpScreenCapture.Save("screen.png");
      return bmpScreenCapture;
    }

    private static Bitmap CreateCommonPixelMask(Bitmap first, Bitmap second, int fault)
    {
      if (first.Size != second.Size)
      {
        throw new ArgumentException("Provided bitmaps have different size");
      }

      var result = new Bitmap(first.Width, first.Height);
      for (var i = 0; i < first.Width; i++)
      {
        for (var j = 0; j < first.Height; j++)
        {
          var firstPxl = first.GetPixel(i, j);
          var secondPxl = second.GetPixel(i, j);
          var pxl = firstPxl.IsEqualsTo(secondPxl, fault) ? firstPxl : Color.Transparent;
          result.SetPixel(i, j, pxl);
        }
      }

      return result;
    }
  }
}
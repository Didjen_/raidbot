using System;
using System.Diagnostics;
using System.Drawing;
using GameBot.Extensions;
using Tesseract;

namespace GameBot.Services
{
  public static class ImageTextReader
  {
    public static string ReadText(Bitmap bmp)
    {
      var converter = new ImageConverter();
      var bmpByteArr = (byte[]) converter.ConvertTo(bmp.ToBlackAndWhite(), typeof(byte[]));

      try
      {
        using (var engine = new TesseractEngine(@"./tessdata", "rus", EngineMode.Default))
        {
          using (var img = Pix.LoadFromMemory(bmpByteArr))
          {
            using (var page = engine.Process(img))
            {
              var text = page.GetText();
              return text;
              // Console.WriteLine("Mean confidence: {0}", page.GetMeanConfidence());
              //
              // Console.WriteLine("Text (GetText): \r\n{0}", text);
              // Console.WriteLine("Text (iterator):");
              // using (var iter = page.GetIterator())
              // {
              //   iter.Begin();
              //
              //   do
              //   {
              //     do
              //     {
              //       do
              //       {
              //         do
              //         {
              //           if (iter.IsAtBeginningOf(PageIteratorLevel.Block))
              //           {
              //             Console.WriteLine("<BLOCK>");
              //           }
              //
              //           Console.Write(iter.GetText(PageIteratorLevel.Word));
              //           Console.Write(" ");
              //
              //           if (iter.IsAtFinalOf(PageIteratorLevel.TextLine, PageIteratorLevel.Word))
              //           {
              //             Console.WriteLine();
              //           }
              //         } while (iter.Next(PageIteratorLevel.TextLine, PageIteratorLevel.Word));
              //
              //         if (iter.IsAtFinalOf(PageIteratorLevel.Para, PageIteratorLevel.TextLine))
              //         {
              //           Console.WriteLine();
              //         }
              //       } while (iter.Next(PageIteratorLevel.Para, PageIteratorLevel.TextLine));
              //     } while (iter.Next(PageIteratorLevel.Block, PageIteratorLevel.Para));
              //   } while (iter.Next(PageIteratorLevel.Block));
              // }
            }
          }
        }
      }
      catch (Exception e)
      {
        Trace.TraceError(e.ToString());
        Console.WriteLine("Unexpected Error: " + e.Message);
        Console.WriteLine("Details: ");
        Console.WriteLine(e.ToString());
      }

      return null;
    }
  }
}
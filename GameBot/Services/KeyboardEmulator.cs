using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace GameBot.Services
{
  //ToDo: may be should rename
  public static class KeyboardEmulator
  {
    [DllImport("user32.dll")]
    private static extern void keybd_event(Keys bVk, byte bScan, UInt32 dwFlags, IntPtr dwExtraInfo);

    private const UInt32 KEYEVENTF_EXTENDEDKEY = 1;
    private const UInt32 KEYEVENTF_KEYUP = 2;

    

    

    public static void PressButton(Keys key)
    {
      keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY, IntPtr.Zero);
      Thread.Sleep(100);
      keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, IntPtr.Zero);
    }

    // PlayerMove("a");
    // PlayerMove("s");
    // PlayerMove("d");
    // PlayerMove("space");
    //
    // PlayerMove("lmb"); //нажать левую кнопку мыши
    // PlayerMove("rmb"); //нажать правую кнопку мыши
    //
    // MyMouseMove(300, 300); //установить курсор на экране в координатах 300 300
  }
}